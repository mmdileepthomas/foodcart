import * as actionTypes from '../actions/actionTypes'
export default (state={items:[], addToCartStatus: false, cartItems: []},action) => {
  switch (action.type) {
    case actionTypes.FETCH_ITEMS:
        return{
          ...state,
          items: action.payload
        }
      break;
    case actionTypes.ENABLE_ADD_REMOVE_BUTTONS:
      return{
        ...state,
        items: state.items.map(obj => {
          if(obj.id === action.payload.itemId){
            return {...obj, status: action.payload.status}
          }
          return obj
        })
      }
    case actionTypes.ADD_ITEM:
      return{
        ...state,
        items: state.items.map(obj => {
            if(obj.id === action.payload.itemId){
              return {...obj, quantity: obj.quantity + 1}
            }
            return obj
          })
      }
    case actionTypes.REMOVE_ITEM:
      return{
         ...state,
         items: state.items.map(obj => {
           if(obj.id === action.payload.itemId && obj.quantity >0){
             return {...obj, quantity: obj.quantity -1}
           }
           return obj
         })
      }
    case actionTypes.CART_ITEMS:
      return{
        ...state,
        cartItems: state.items.copyWithin(obj => {
          if(obj.id === action.payload.itemId){
            return {...obj}
          }
        })
      }
    case actionTypes.REMOVE_CART_ITEM:
      return{
        ...state,
        cartItems: state.cartItems.filter(obj => {
          if(obj.id !== action.payload.itemId){
            return {obj}
          }
        }),
        items: state.items.map(obj => {
          if(obj.id === action.payload.itemId){
            return {...obj , quantity: 0}
          }
          return obj
        })
      }
    default:
      return state
  }
}
