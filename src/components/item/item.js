import React from  'react'
// importing clss
import './item.css'
const item = (props) => {
  return(
    <div className="parent-item-container">
    {props.items.map((item,key)=> {
      return(
              <div className="item-container" key={key}>
                <div className="item-img-container">
                  <img src={item.imgSrc} alt={item.brandName} />
                </div>
                <div className="item-details">
                  <p>{item.brandName}</p>
                  <p>{item.productName}</p>
                  <p>{item.packingDetail}</p>
                  <p>{`Rs: ${item.price}`}</p>
                </div>
                {item.status? null :<button
                onClick={() => props.clicked(item.id)}>
                {(item.status == undefined)?
                    'Add To Cart' : (item.quantity > 0)?
                    `${item.quantity} in cart`
                    :"Nothing Added" }
                </button>}
                {item.status?
                  (<div className="button-container">
                    <button onClick={() =>props.removeItem(item.id)}>-</button>
                    <button onClick={() => props.clicked(item.id)}>{(item.status == undefined)? 'Add To Cart' : (item.quantity > 0)? `${item.quantity} in cart`  :" No  Item's" }</button>
                    <button onClick={() => props.addItem(item.id)}>+</button>
                  </div>) : null }
              </div>
            )
     })}
     </div>
  )
}

export default item
