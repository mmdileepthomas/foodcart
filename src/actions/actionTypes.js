// UI actions
export const FETCH_UI_HEADINGS = 'fetch_ui_headings'
export const FETCH_ITEMS = 'fetch_items'
export const ENABLE_ADD_REMOVE_BUTTONS = 'enable_add_remove_buttons'
export const ADD_ITEM = 'add_item'
export const REMOVE_ITEM = 'remove_item'
export const CART_ITEMS = 'cart_items'
export const REMOVE_CART_ITEM = 'remove_cart_item'
export const FETCH_QUANTITY_TOTAL = 'fetch_quantity_total'
